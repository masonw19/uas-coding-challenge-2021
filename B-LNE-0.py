'''
RULES:
    - The points of the line won't be inside the circle
    - Coordinates and radiuses will be integer values
    - The line can intersect one or more times
'''

def find_intersections(line, circle):
    """
    find_intersections returns the intersecting points between the line
    and the circle

    :param line: list of two coordinates which the line will pass through 
    :param circle: a list with center coordinates and the radius of the circle
    :return: returns the a list of points that the line intersects the circle at
    """ 
    
    p1_x, p1_y = line[0][0], line[0][1]
    p2_x, p2_y = line[1][0], line[1][1]

    circ_x, circ_y = circle[0][0], circle[0][1]
    circ_r = circle[1]

    # circle equation:  circ_r^2 = (x-circ_x)^2 + (y-circ_y)^2
    # line equation:    y = m(x-p1_x) + p1_y

    # check for infinite slope
    if p2_x - p1_x == 0:
        x = p1_x
        if circ_r ** 2 - (x - circ_x) ** 2 < 0:
            print("no real roots")
            return None
        elif circ_r ** 2 - (x - circ_x) ** 2 == 0:
            y = circ_y + (circ_r ** 2 - (x - circ_x) ** 2) ** (1 / 2) 
            print("one real root")
            return [x,y]
        else:
            y_1 = circ_y + (circ_r ** 2 - (x - circ_x) ** 2) ** (1 / 2)
            y_2 = circ_y - (circ_r ** 2 - (x - circ_x) ** 2) ** (1 / 2)
            print("two real roots")
            return [[x,y_1],[x,y_2]]

    m = (p2_y - p1_y) / (p2_x - p1_x)

    # Substitution: 0 = (x-circ_x)^2 + (m(x-p1_x) + p1_y - circ_y)^2 - circ_r^2
    # Simplify:     0 = x^2 - 2 * circ_x * x + circ_x^2  + (m*x-p1_x*m + p1_y - circ_y)^2 - circ_r^2
    # Simlplify:    0 = x^2 - 2 * circ_x * x + circ_x^2 + (circ_y^2) - (2 * p1_y * circ_y) + (p1_y^2) + (2 * p1_x * circ_y * m) 
    #                  - (2 * p1_x * p1_y * m) + (p1_x^2 * m^2) - (2 * x * circ_y * m) + (2 * x * p1_y * m) - (2 * x * p1_x * m^2) + (x^2 * m^2) - circ_r^2
    
    # Quadratic Equation: a * x^2 + b * x + c = 0
    a = 1 + m ** 2 
    b = -2 * circ_x - 2 * circ_y * m + 2 * p1_y * m - 2 * p1_x * m ** 2
    c = circ_x ** 2 + circ_y ** 2 - (2 * p1_y * circ_y) + (p1_y ** 2) + (2 * p1_x * circ_y * m) - (2 * p1_x * p1_y * m) + (p1_x ** 2 * m ** 2) - circ_r ** 2

    # Quadratic Formula: (-b +/- sqrt(b^2 - 4ac)) / (2a) 
    if b ** 2 - 4 * a * c < 0:
        print("no real roots")
        return None
    elif b ** 2 - 4 * a * c == 0:
        x = (-b  + (b ** 2 - 4 * a * c) ** (1 / 2)) / (2 * a)
        y = m * (x - p1_x) + p1_y
        p = [x, y]

        print("one real root")
        return [x,y]
    else:
        x_1 = (-b  + (b ** 2 - 4 * a * c) ** (1 / 2)) / (2 * a) 
        y_1 = m * (x_1 - p1_x) + p1_y

        x_2 = (-b  - (b ** 2 - 4 * a * c) ** (1 / 2)) / (2 * a) 
        y_2 = m * (x_2 - p1_x) + p1_y

        print("two real roots")
        return [[x_1,y_1],[x_2,y_2]] 

def main():
    # Case 1: two intersections case
    assert find_intersections(line=[[1,2], [2,3]], circle=[[5,5], 1])           == \
        [[5.0, 6.0], [4.0, 5.0]], "Case 1: Intersections should be [[5.0, 6.0], [4.0, 5.0]]"

    # Case 2: one intersection, 0 line slope
    assert find_intersections(line=[[19,6], [6,6]], circle=[[5,5], 1])          == \
        [5.0, 6.0], "Case 2: Intersections should be [5.0, 6.0]"

    # Case 3: one  intersection, infinite line slope    
    assert find_intersections(line=[[10,2], [10,3]], circle=[[0,0], 10])        == \
        [10.0, 0.0], "Case 3: Intersections should be [10.0, 0.0]"

    # Case 4: two intersections, 0 line slope   
    assert find_intersections(line=[[1,0], [2,0]], circle=[[0,0], 1])           == \
        [[1.0, 0.0], [-1.0, 0.0]], "Case 4: Intersections should be [[1.0, 6.0], [-1.0, 0.0]]"

    # Case 5: two  intersection, infinite line slope    
    assert find_intersections(line=[[7,2], [7,3]], circle=[[0,0], 10])          == \
        [[7.0, 7.14142842854285], [7.0, -7.14142842854285]], "Case 5: Intersections should be [[7.0, 7.14142842854285], [7.0, -7.14142842854285]]"
    
    # Case 6: no intersection, 0 line slope 
    assert find_intersections(line=[[1,2], [2,2]], circle=[[0,0], 1])           == \
        None, "Case 6: Intersections should be None"

    # Case 7: no intersections, infinite line slope 
    assert find_intersections(line=[[6,2], [6,3]], circle=[[2,2], 2])           == \
        None, "Case 7: Inteserctions should be None"
    
    # Case 8: two intersections
    assert  find_intersections(line=[[99,99], [100,100]], circle=[[0,0], 1])    == \
        [[0.7071067811865476, 0.7071067811865532], [-0.7071067811865476, -0.7071067811865532]], "Case 8: Intersections should be [0.7071067811865476, 0.7071067811865532], [-0.7071067811865476, -0.7071067811865532]]"
    
    # Case 9: no intersections
    assert find_intersections(line=[[99,99], [100,100]], circle=[[1,3], 1])     == \
        None, "Case 9: Intersections should be None"

    print("SUCCESS! All test cases passed.")

if __name__ == '__main__':
    main()
